;; -*- coding: utf-8 -*-
;; Time-stamp: <Chen Wang: 2018-05-31 13:54:05>

(require 'meghanada)
(add-hook 'java-mode-hook
          (lambda ()
            ;; meghanada-mode on
            (meghanada-mode t)
            (flycheck-mode +1)
            (rainbow-delimiters-mode t)
            (smartparens-mode t)
            (hs-minor-mode t)
            (auto-complete-mode -1)
            (electric-pair-mode t)
            (electric-operator-mode t)
            (setq c-basic-offset 2)
            ;; use code format
            (add-hook 'before-save-hook 'meghanada-code-beautify-before-save)))


(add-to-list 'auto-mode-alist '("\\.gradle\\'" . groovy-mode))
