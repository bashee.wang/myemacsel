;; -*- coding: utf-8 -*-
;;Time-stamp: <Chen Wang: 2018-02-12 23:30:49>

;; 设置是否有备份文件
;; (setq make-backup-files nil)
;; backup policies
(setq make-backup-files t)
(setq version-control t)
;; (setq kept-old-versions 2)
;; (setq kept-new-versions 5)
(setq delete-old-versions t)
(setq dired-kept-versions 1)
;; 设置备份目录
(setq backup-directory-alist '(("." . "~/.emacs.d/backup")))
;; (setq backup-by-copying t)
;;Preserve hard links to the file you'e editing
;; (setq backup-by-copying-when-linked t)
;;Preserve the owner and group of the file you'e editing
;; (setq backup-by-copying-when-mismatch t)

;;;auto save interval time -- 60s
(setq auto-save-interval 600) 
