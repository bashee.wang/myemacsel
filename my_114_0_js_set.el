;; -*- coding: utf-8 -*-
;; Time-stamp: <Chen Wang: 2019-06-26 12:01:23>

;; (add-to-list 'auto-mode-alist '("\\.js\\'" . js-mode))
;; (add-to-list 'interpreter-mode-alist '("node" . js2-mode))
;; (add-to-list 'auto-mode-alist '("\\.jsx?\\'" . js2-jsx-mode))
;; (add-to-list 'interpreter-mode-alist '("node" . js2-jsx-mode))

(add-hook 'js-mode-hook 'ac-js2-mode)
(add-hook 'js-mode-hook 'electric-operator-mode)
(add-hook 'js-mode-hook 'rainbow-delimiters-mode)
(add-hook 'js-mode-hook 'hs-minor-mode)
(add-hook 'js-mode-hook 'js2-minor-mode)

(add-hook 'js-mode-hook
          (lambda () 
            (define-key js2-mode-map "\C-c\C-v" 'hs-toggle-hiding)))


(setq ac-js2-evaluate-calls t)
;; (setq ac-js2-external-libraries '("full/path/to/a-library.js"))

(require 'js2-refactor)
(add-hook 'js-mode-hook #'js2-refactor-mode)
(js2r-add-keybindings-with-prefix "C-c C-m")

