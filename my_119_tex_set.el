;; -*- coding: utf-8 -*-
;; Time-stamp: <Chen Wang: 2018-12-11 21:56:36>

(load "auctex.el" nil t t)
(load "preview.el" nil t t)

(setq TeX-auto-save t)
(setq TeX-parse-self t)
(setq-default TeX-master nil)

(setq TeX-source-correlate-mode t)
(setq TeX-source-correlate-start-server t)
;; (setq TeX-view-program-list '(("Okular" "okular --unique %o#src:%n%b")
;;                               ("Skim" "displayline -b -g %n %o %b")))
;; (setq TeX-view-program-selection
;;       (quote
;;        ((output-pdf "Zathura")
;;         (output-dvi "Okular")
;;         (output-html "xdg-open"))))
