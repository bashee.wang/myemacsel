;; -*- coding: utf-8 -*-
;; Time-stamp: <Chen Wang: 2019-06-21 16:03:33>

;; (setq inferior-julia-program-name "/home/coeus/src/julia/julia")

;; (void-variable ess-mode-map)

(require 'ess-site)
(defun then_R_operator ()
  "R - %>% operator or 'then' pipe operator"
  (interactive)
  (just-one-space 1)
  (insert "%>%")
  (reindent-then-newline-and-indent))

(defun R_in_operator ()
  "R - %in% operator"
  (interactive)
  (just-one-space 1)
  (insert "%in%")
  (just-one-space 1))
  ;; (reindent-then-newline-and-indent))
(define-key ess-mode-map (kbd "C-<") 'R_in_operator)
(define-key inferior-ess-mode-map (kbd "C-<") 'R_in_operator)

(define-key ess-mode-map (kbd "C-%") 'then_R_operator)
(define-key inferior-ess-mode-map (kbd "C-%") 'then_R_operator)
(define-key ess-mode-map (kbd "C-.") 'then_R_operator)
(define-key inferior-ess-mode-map (kbd "C-.") 'then_R_operator)

;; ;; customized shortkeys
(define-key ess-mode-map (kbd "C-,") 'ess-insert-assign)
(define-key inferior-ess-mode-map (kbd "C-,") 'ess-insert-assign)
(define-key ess-mode-map (kbd "C-=") 'ess-insert-assign)
(define-key inferior-ess-mode-map (kbd "C-=") 'ess-insert-assign)

;; ;; prettify
(add-hook 'ess-mode-hook
          (lambda ()
            (push '(":)" . ?☺) prettify-symbols-alist)
            (push '(":(" . ?☹) prettify-symbols-alist)
            (push '("%>%" . ?⮞) prettify-symbols-alist)
            ))

(add-hook 'ess-mode-hook 'electric-spacing-mode)
(add-hook 'ess-mode-hook 'rainbow-delimiters-mode)
(add-hook 'ess-mode-hook 'rainbow-mode)
(add-hook 'ess-mode-hook 'linum-mode)
(add-hook 'ess-mode-hook 'turn-on-prettify-symbols-mode)

(setq ess-smart-S-assign-key ":")
(ess-toggle-S-assign nil)
(ess-toggle-S-assign nil)
(ess-toggle-underscore nil) ; leave underscore key alone!

(add-hook 'ess-mode-hook
          (lambda ()
            ess-indent-with-fancy-comments 'nil))
(setq ess-indent-with-fancy-comments nil)

(setq ess-R-font-lock-keywords
      '((ess-R-fl-keyword:modifiers . t)
        (ess-R-fl-keyword:fun-defs . t)
        (ess-R-fl-keyword:keywords . t)
        (ess-R-fl-keyword:assign-ops . t)
        (ess-R-fl-keyword:constants . t)
        (ess-fl-keyword:fun-calls . t)
        (ess-fl-keyword:numbers)
        (ess-fl-keyword:operators . t)
        (ess-fl-keyword:delimiters)
        (ess-fl-keyword:= . t)
        (ess-R-fl-keyword:F&T)
        (ess-R-fl-keyword:%op% .t )))
(add-hook 'ess-mode-hook 'turn-on-pretty-mode)

;; highlights FIXME: TODO: and BUG: in prog-mode
(add-hook 'prog-mode-hook
          (lambda ()
            (font-lock-add-keywords nil
                                    '(("\\<\\(YT\\|FIXME\\|TODO\\|BUG\\):" 1 font-lock-warning-face t)))))


;; R markdown mode
(defun rmd-mode()
  "ESS markdown mode for rmd files"
  (interactive)
  (require 'poly-R)
  (require 'poly-markdown)
  (poly-markdown+r-mode)
  )
