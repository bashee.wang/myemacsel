;; -*- coding: utf-8 -*-
;; Time-stamp: <Chen Wang: 2019-07-10 16:25:39>

(load "/home/coeus/usr/local/texlive/2019/texmf-dist/asymptote/asy-mode.el")

(autoload 'asy-mode "asy-mode.el" "Asymptote major mode." t)
(autoload 'lasy-mode "asy-mode.el" "hybrid Asymptote/LaTeX major mode." t)
(autoload 'asy-insinuate-latex "asy-mode.el" "Asymptote insinuate LaTeX." t)
(add-to-list 'auto-mode-alist '("\\.asy$" . asy-mode))

(add-hook 'asy-mode-hook 'rainbow-delimiters-mode)
(add-hook 'asy-mode-hook 'py-smart-operator-mode)
(add-hook 'asy-mode-hook 'auto-complete-mode)

(require 'irony)
(add-to-list 'irony-supported-major-modes 'asy-mode)
