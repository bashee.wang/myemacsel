;; -*- coding: utf-8 -*-

;; This file is used to set some convienet seetings for Emacs
;; Time-stamp: <Chen Wang: 2019-06-14 14:41:41>

;; setup various dot file's location
(setq tags-table-list '(~/.emcas.d/))
(setq bookmark-default-file "~/.emacs.d/_emacs_bmk")
(setq abbrev-file-name "~/.emacs.d/_abbrev_defs")
(setq custom-file "~/.emacs.d/_emacs_custom.el")
(load-file "~/.emacs.d/_emacs_custom.el")
;; (setq bbdb-file "~/.emacs.d/_bbdb")
;; (setq todo-file-do "~/.emacs.d/todo-do")
;; (setq todo-file-done "~/.emacs.d/todo-done")
;; (setq todo-file-top "~/.emacs.d/todo-top")
;; (setq diary-file "~/.emacs.d/_diary.gpg")

(setq user-full-name "Chen Wang")

(add-to-list 'Info-default-directory-list "~/.emacs.d/info/")

(setenv "PATH" (concat (getenv "PATH") ":/home/coeus/usr/local/texlive/2019/bin/x86_64-linux"))

;; 打开关闭一些特性
(minibuffer-electric-default-mode 1)
(auto-compression-mode 1)
;; (auto-fill-mode 1)
;; (icomplete-mode 1)
;; (ido-mode 1)
(server-mode 1)
(mouse-avoidance-mode 'exile)

;; about mode line
(column-number-mode 1)
(blink-cursor-mode -1)
(display-time-mode 1)
(size-indication-mode 1)
(display-battery-mode 1)

;; some convenient settings
(setq toggle-truncate-line t)

;; Auto revert buffer if changes outside.
(auto-revert-mode 1)
(setq auto-revert-mode t)
(setq global-auto-revert-mode t)
(setq adaptive-fill-mode nil)

;(setq-default kill-whole-line t)
(setq debug-on-error t)
(fset 'yes-or-no-p 'y-or-n-p)
(setq mouse-yank-at-point t)
(setq ansi-color-for-comint-mode t)
(setq-default truncate-partial-width-windows nil)
(setq max-specpdl-size 6800)
(setq default-fill-column 100)
(setq-default fill-column 80)
(setq transient-mark-mode t)
(transient-mark-mode 1)

(setq delete-selection-mode t)
(setq x-select-enable-clipboard t)
;;(setq interprogram-paste-function 'x-cut-buffer-or-selection-value)
;;鼠标选中即复制
(setq mouse-drag-copy-region t)

;;相同文件名，显示路径
(setq uniquify-buffer-name-style 'forward)
(setq-default truncate-partial-width-windows nil)

;;高亮搜索、替代内容
(setq search-highlight t)   
(setq query-replace-highlight t)  

;;设定打开文件时的默认目录
(setq default-directory "~/")

;;关闭出错时的提示音
(setq visible-bell t)

;;set grep command options
(setq grep-command "grep -i -nH -e")

;;redo次数
(setq kill-ring-max 1000)

;;No hints for M-x
;; (setq suggest-key-bindings nil)

;;specify printing format
(setq ps-paper-type 'a4)

;;Tab的有关设置
(setq-default indent-tabs-mode nil)
(setq default-tab-width 4)
(setq tab-stop-list ())


;;可以递归的使用 minibuffer。
(setq enable-recursive-minibuffers t)

;;防止页面滚动时跳动
(setq scroll-step 1
      scroll-margin 3
      scroll-conservatively 10000)
      
;;把缺省的 major mode 设置为 text-mode
(setq default-major-mode 'text-mode)
(add-hook 'text-mode-hook 'turn-on-auto-fill)

;;括号匹配
(show-paren-mode t)
(setq show-paren-style 'mixed)

;;设置标题栏
(setq frame-title-format "%b@%z|%t %f@size:%I")

;;语法高亮
(global-font-lock-mode t)

;; 去掉工具栏，保持菜单，去掉滚动栏
(tool-bar-mode -1)
;; (menu-bar-mode 0)
(scroll-bar-mode -1)

;; 关闭菜单栏，但是按f10可以挑出菜单20秒
;; (global-set-key [f10] (lambda () (interactive)
;;   (if menu-bar-mode
;;     (menu-bar-open)
;;     (run-with-idle-timer 20 nil (lambda () ;;20 sec
;;        (menu-bar-mode 0)
;;        (set-frame-size
;;         (selected-frame)(frame-width)(+ (frame-height) 1))));;1 is enough(sometime?),if full screen need 2
;;            (menu-bar-mode 1)
;;            (set-frame-size (selected-frame)(frame-width)(-
;;               (frame-height) 1))
;;            (run-with-idle-timer 0.5 nil 'menu-bar-open);; not work if directly call (menu-bar-open),
;;            )))

;; setting Abbrev-mode
(setq-default abbrev-mode t)
; (read-abbrev-file "~/.emacs.d/_abbrev_defs")
(setq save-abbrevs t)

;;打开一些缺省禁用的功能
(put 'set-goal-column 'disabled nil)
(put 'narrow-to-region 'disabled nil)
(put 'upcase-region 'disabled nil)
(put 'downcase-region 'disabled nil)
(put 'capitalize-region 'disabled nil)
;;关闭一些默认打开的功能
(put 'overwrite-mode 'disabled t)

;; 防止不小心按到菜单中的 print 时，emacs 死掉
(fset 'print-buffer 'ignore)
(setq lpr-command "")
(setq printer-name "")

;; 自动显示图片
(setq auto-image-file-mode t)

;;在行尾上下移动的光标，始终保持在行尾
(setq line-move-visual nil)
(setq track-eol t)

;; 关闭开机画面
(setq inhibit-startup-message t)
(setq initial-scratch-message nil)

;; 设置 sentence-end 可以识别中文。
(setq sentence-end "\\([。！？]\\|……\\|[.?!][]\"')}]*\\($\\|[ \t]\\)\\)[ \t\n]*")

;;显示时间
(setq display-time-24hr-format t)
(setq display-time-day-and-date t)
(display-time)

;; 设置 hippie-expand 的行为，一般我用 hippie-expand 比较多。如果
;; hippie-expan 给出的结果不好，就改用 dabbrev-expand。所以改成使用相同
;; 的 Alt。
(setq hippie-expand-try-functions-list
      '(try-expand-line
        try-expand-dabbrev
        try-expand-line-all-buffers
        try-expand-list
        try-expand-list-all-buffers
        try-expand-dabbrev-visible
        try-expand-dabbrev-all-buffers
        try-expand-dabbrev-from-kill
        try-complete-file-name
        try-complete-file-name-partially
        try-complete-lisp-symbol
        try-complete-lisp-symbol-partially
        try-expand-whole-kill))
(global-set-key (kbd "M-;") 'hippie-expand)
(global-set-key (kbd "M-/") 'dabbrev-expand)

;;;;;;;Desktop -- restore all files after reopen the emacs
(setq my-desktop-dir "~/.emacs.d/server")
(setq desktop-base-file-name "emacs.desktop")
(setq desktop-path (list my-desktop-dir))
(desktop-save-mode 1)

;; 自动在文件头加入 time-stamp。格式参考 time-stamp 函数的说明。
(setq time-stamp-pattern nil)
(add-hook 'before-save-hook 'time-stamp)
(setq time-stamp-active t)
(setq time-stamp-warn-inactive t)
(add-hook 'write-file-hooks 'time-stamp)
(setq time-stamp-format "%U: %:y-%02m-%02d %02H:%02M:%02S")

;;; Set Sunrise and Sunset place for Calendar
;; For Chengdu
(setq calendar-latitude 30.67)
(setq calendar-longitude 104.06)
(setq calendar-location-name "Chengdu, Sichuan, China")
;; For Israel
;; (setq calendar-latitude 32.795356)
;; (setq calendar-longitude 34.959869)
;; (setq calendar-location-name "Haifa, Israel")
(add-hook 'calendar-mode-hook  
	  (lambda ()
	    (define-key calendar-mode-map "e" 'mark-diary-entries)))

(setq woman-use-own-frame nil)  ;;设置woman只用一个frame
(setq message-log-max t)  ;;设置message记录全部消息, 而不用截去

;;Tramp settings
;; (require 'tramp)
;; (setq tramp-default-method "plink")

;;remove extra whitespace when save a file.
(add-hook 'before-save-hook 'whitespace-cleanup nil t)

;; set split width threshold to 140 to fit T400 - not OK...
;; Original value is 160.
;; (setq split-width-threshold 160)

;; 拷贝代码自动格式化。
(dolist (command '(yank yank-pop))
  (eval
   `(defadvice ,command (after indent-region activate)
      (and (not current-prefix-arg)
           (member major-mode
                   '(emacs-lisp-mode
                     lisp-mode
                     clojure-mode
                     scheme-mode
                     haskell-mode
                     ruby-mode
                     rspec-mode
                     python-mode
                     c-mode
                     c++-mode
                     objc-mode
                     latex-mode
                     js-mode
                     plain-tex-mode))
           (let ((mark-even-if-inactive transient-mark-mode))
             (indent-region (region-beginning) (region-end) nil)))))) 
