;; -*- coding: utf-8 -*-
;; Time-stamp: <Chen Wang: 2018-08-24 17:02:52>

;; (add-to-list 'package-archives
;;              '("elpy" . "http://jorgenschaefer.github.io/packages/"))
;; (setq package-archives
;;       '(
;;         ("gnu"   . "http://mirrors.tuna.tsinghua.edu.cn/elpa/gnu/")
;;         ("melpa" . "http://mirrors.tuna.tsinghua.edu.cn/elpa/melpa/")
;;         ("Marmalade" . "http://mirrors.tuna.tsinghua.edu.cn/elpa/marmalade/")
;;         ("Org" . "http://mirrors.tuna.tsinghua.edu.cn/elpa/org/")
;;         ("elpy" . "http://jorgenschaefer.github.io/packages/")
;;         )
;;       )

;; (setq package-archives
;;       '(
;;         ("gnu" . "http://elpa.gnu.org/packages/")
;;         ("elpy" . "http://jorgenschaefer.github.io/packages/")
;;         ;; ("marmalade" . "http://marmalade-repo.org/packages/")
;;         ("melpa" . "http://mirrors.tuna.tsinghua.edu.cn/elpa/melpa/")
;;         ("org" . "http://orgmode.org/elpa/")
;;         )
;;       )


(setq package-archives
      '(
        ("gnu" . "http://elpa.gnu.org/packages/")
        ("elpy" . "http://jorgenschaefer.github.io/packages/")
        ;; ("marmalade" . "http://marmalade-repo.org/packages/")
        ("melpa" . "http://melpa.milkbox.net/packages/")
        ("org" . "http://orgmode.org/elpa/")        
        )
      )

;; (setq package-enable-at-startup nil)
;; (package-initialize)

;; set paradox token
;; (require 'paradox)
;; (paradox-enable)
;; (setq paradox-github-token "ba52483fdb10f99a04b2b5210a69ceeab897eede")
