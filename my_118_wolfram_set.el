;; -*- coding: utf-8 -*-
;; Time-stamp: <Chen Wang: 2018-05-29 15:56:02>

(autoload 'wolfram-mode "wolfram-mode" nil t)
(autoload 'run-wolfram "wolfram-mode" nil t)
(setq wolfram-program "/usr/local/bin/MathKernel")
(add-to-list 'auto-mode-alist '("\\.m$" . wolfram-mode))
(setq wolfram-path "~/.Mathematica/Applications") ;; e.g. on Linux ~/.Mathematica/Applications

(setq wolfram-alpha-app-id "JAAY8Y-VKYHEU7J9K")
