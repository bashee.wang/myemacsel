;; -*- coding: utf-8 -*-
;; Time-stamp: <Chen Wang: 2016-12-24 22:36:45>

(require 'helm-config)
(global-set-key (kbd "M-x") 'helm-M-x)

(global-set-key (kbd "C-x b") 'helm-mini)
(global-set-key (kbd "C-x C-f") 'helm-find-files)
