;; -*- coding: utf-8 -*-
;; Time-stamp: <Chen Wang: 2018-11-20 19:14:19>

(add-hook 'c++-mode-hook 'irony-mode)
(add-hook 'c-mode-hook 'irony-mode)
(add-hook 'objc-mode-hook 'irony-mode)


;; replace the `completion-at-point' and `complete-symbol' bindings in
;; irony-mode's buffers by irony-mode's function
(defun my-irony-mode-hook ()
  (define-key irony-mode-map [remap completion-at-point]
    'irony-completion-at-point-async)
  (define-key irony-mode-map [remap complete-symbol]
    'irony-completion-at-point-async))
(add-hook 'irony-mode-hook 'my-irony-mode-hook)
(add-hook 'irony-mode-hook 'irony-cdb-autosetup-compile-options)

(eval-after-load 'company
  '(add-to-list 'company-backends 'company-irony))

(add-hook 'irony-mode-hook
          (lambda ()
            (define-key irony-mode-map (kbd "<backtab>") 'hs-toggle-hiding)
            (define-key irony-mode-map (kbd "C-c h") 'hs-hide-all)
            (define-key irony-mode-map (kbd "C-c s") 'hs-show-all)
            ))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; this fragment originally came from the web somewhere, but the outline-regexp
;; was horribly broken and is broken in all instances of this code floating
;; around. Finally fixed by Charl P. Botha <<a href="http://cpbotha.net/">http://cpbotha.net/</a>>
(defun my-cpp-hook ()
  ;; turn on outline mode
  (outline-minor-mode t)
  (linum-mode t)
  (hs-minor-mode t)
  ;; (ac-ropemacs-setup)
  ;; initially hide all but the headers
  ;; (hide-body)
  (show-paren-mode 1)
  (company-mode 1)
  (rainbow-delimiters-mode 1)
  (rainbow-mode 1)
  ;; (semantic-mode 1)
  )

(add-hook 'c++-mode-hook 'my-cpp-hook)

;; (require 'clang-format)
(setq clang-format-executable "clang-format-3.5")

;; Set default intention to 4 space
(setq-default c-basic-offset 4)

;; Whenever you type certain characters, a newline will be inserted automatically. 
(add-hook 'c-mode-common-hook '(lambda () (c-toggle-auto-state 1)))

;; (setq semantic-default-submodes
;;       '(global-semantic-idle-scheduler-mode
;;         global-semanticdb-minor-mode
;;         global-semantic-idle-summary-mode
;;         global-semantic-idle-completions-mode
;;         global-semantic-highlight-func-mode
;;         global-semantic-decoration-mode
;;         global-semantic-mru-bookmark-mode))
