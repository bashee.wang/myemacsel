;; -*- coding: utf-8 -*-
;; Time-stamp: <Chen Wang: 2017-04-18 17:36:35>

;; Sort by path name
;; (define-ibuffer-sorter filename-or-dired
;;   "Sort the buffers by their pathname."
;;   (:description "filenames plus dired")
;;   (string-lessp 
;;    (with-current-buffer (car a)
;;      (or buffer-file-name
;;          (if (eq major-mode 'dired-mode)
;;              (expand-file-name dired-directory))
;;          ;; so that all non pathnames are at the end
;;          "~"))
;;    (with-current-buffer (car b)
;;      (or buffer-file-name
;;          (if (eq major-mode 'dired-mode)
;;              (expand-file-name dired-directory))
;;          ;; so that all non pathnames are at the end
;;          "~"))))
;; (define-key ibuffer-mode-map (kbd "s p")     'ibuffer-do-sort-by-filename-or-dired)


;; Use human readable Size column instead of original one
;; (define-ibuffer-column size-h
;;   (:name "Size" :inline t)
;;   (cond
;;    ((> (buffer-size) 1000000) (format "%7.1fM" (/ (buffer-size) 1000000.0)))
;;    ((> (buffer-size) 100000) (format "%7.0fk" (/ (buffer-size) 1000.0)))
;;    ((> (buffer-size) 1000) (format "%7.1fk" (/ (buffer-size) 1000.0)))
;;    (t (format "%8d" (buffer-size)))))

;; Modify the default ibuffer-formats
;; (setq ibuffer-formats
;;       '((mark modified read-only " "
;;               (name 18 18 :left :elide)
;;               " "
;;               (size-h 9 -1 :right)
;;               " "
;;               (mode 16 16 :left :elide)
;;               " "
;;               filename-and-process)))

;; To change the width of column "Name"
(setq ibuffer-formats
      '((mark modified read-only " "
              (name 30 30 :left :elide) " "
              (size 9 -1 :right) " "
              (mode 16 16 :left :elide) " " filename-and-process)
        (mark " " (name 16 -1) " " filename)))

(setq ibuffer-formats
      '((mark modified read-only
              " " (name 30 30)
              " " (size 9 -1 :right)
              " " (mode 16 16 :left)
              ;; " " (process 10 -1)
              " " filename-and-process)
        (mark " " (name 30 -1) " " filename))
      ibuffer-elide-long-columns t
      ibuffer-eliding-string "&")


;;;; Ibuffer-mode group
;; use *list-colors-display* to get colors
(defface ibuffer-sql-face '((t (:foreground "LightGoldenrod")))
  "Ibuffer sql face")
(defface ibuffer-xml-face '((t (:foreground "SlateBlue1")))
  "Ibuffer xml face")
(defface ibuffer-html-face '((t (:foreground "SlateBlue1")))
  "Ibuffer html face")
(defface ibuffer-text-face '((t (:foreground "SkyBlue")))
  "Ibuffer text face")
(defface ibuffer-readonly-face '((t (:foreground "Blue" :weight bold)))
  "Ibuffer read only face")
(defface ibuffer-dired-face '((t (:foreground "plum1" :weight bold)))
  "Ibuffer dired face")
(defface ibuffer-shell-face '((t (:foreground "maroon" :weight bold)))
  "Ibuffer shell face")
(defface ibuffer-lisp-face '((t (:foreground "gold")))
  "Ibuffer lisp face")
;; (defface ibuffer-irc-face '((t (:foreground "red")))
;;     "Ibuffer IRC face")
(defface ibuffer-tex-face '((t (:foreground "deeppink")))
  "Ibuffer TeX face")
(defface ibuffer-conf-face '((t (:foreground "seegreen1")))
  "Ibuffer Conf face")
(defface ibuffer-org-face '((t (:foreground "cyan")))
  "Ibuffer org face")
(defface ibuffer-python-face '((t (:foreground "green")))
  "Ibuffer Python face")
(defface ibuffer-perl-face '((t (:foreground "purple1")))
  "Ibuffer perl face")
;; (defface ibuffer-text-face '((t (:foreground "white")))
;;     "Ibuffer text face")

(setq
 ibuffer-fontification-alist
 '(;; Sql-mode buffers
   (5  (string-match ".sql$" (buffer-name)) ibuffer-sql-face)
   (5  (string-match ".xml$" (buffer-name)) ibuffer-xml-face)   
   (5  (string-match ".org$" (buffer-name)) ibuffer-org-face)
   (5  (string-match ".pl$" (buffer-name)) ibuffer-perl-face)
   (5  (string-match ".pm$" (buffer-name)) ibuffer-perl-face)
   (5  (string-match ".py$" (buffer-name)) ibuffer-python-face)
   (5  (string-match ".c??$" (buffer-name)) ibuffer-sql-face)
   (5  (string-match ".html?$" (buffer-name)) ibuffer-html-face)
   ;; (5  (string-match ".txt$" (buffer-name)) ibuffer-text-face)
   (10 (eq major-mode 'text-mode) ibuffer-text-face)
   (10 (eq major-mode 'dired-mode) ibuffer-dired-face)
   (15 (eq major-mode 'emacs-lisp-mode) ibuffer-lisp-face)
   ;; (20 (eq major-mode 'rcirc-mode) ibuffer-irc-face)
   ;; (25 (eq major-mode 'erc-mode) ibuffer-irc-face)
   (30 (eq major-mode 'latex-mode) ibuffer-tex-face)
   (35 (eq major-mode 'conf-mode) ibuffer-conf-face)
   ;; (40 (eq major-mode 'Python-mode) ibuffer-muse-face)
   (45 (string-match "^*" (buffer-name)) ibuffer-readonly-face)
   (45 (string-match "shell:" (buffer-name)) ibuffer-shell-face)
   ))

(add-hook 'ibuffer-mode-hook
          (lambda ()
            (setq ibuffer-filter-groups
                  '(
                    ("Dired Folders" (mode . dired-mode))
                    ("Daily Planner" (or
                                      (name . "^\\*Calendar\\*$")
                                      (name . ".*diary.*")
                                      (mode . org-mode)))
                    ("Emacs LISP Files" (or
                                          (name . "\.elc?$")
                                          (name . "^\.emacs.*")))
                    ("Python Files" (or
                                          (mode . python-mode)))
                    ("C/C++ Files" (or
                                    (mode . c++-mode)
                                    (mode . c-mode)
                                    ))
                    ("Web Mode Files" (or
                                       (name . "\.html?$")
                                       (name . "\.css?$")))
                    ;; ("SQL Mode Files" (or
                    ;;                    (name . "\.sql?$")
                    ;;                    ;; (name . "^\\*SQL\\*$")
                    ;;                    ))
                    ;; ("Perl Mode Files" (or
                    ;;                     (mode . cperl-mode))) 
                    ;; ("Script" (or
                    ;;            (mode . powershell-mode))) 
                    ;; ("Work Related Files" (or
                    ;;                        (name . "\.xml$")
                    ;;                        (name . "\..?tpl$")
                    ;;                        (name . "\.env$")
                    ;;                        (name . "\.plist$")
                    ;;                        (name . "\.cfg$")
                    ;;                        (name . "\.ph$")
                    ;;                        (name . "\.h$")
                    ;;                        (name . "\.cpp$")                             
                    ;;                        (name . "\.usrv$")                             
                    ;;                        (name . "[1-9][A-Z]")
                    ;;                        (name . "^\\*Bookmark.*\\*$")
                    ;;                        (name . "\.soc$")))
                    ("*Shell Process*" (or
                                        (mode . shell-mode)))
                    ("*READ_ONLY*" (name . "\\*.*\\*"))
                    ))))

