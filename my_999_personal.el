;; -*- coding: utf-8 -*-
;; Time-stamp: <Chen Wang: 2017-01-04 14:04:05>

;; Removes *scratch* from buffer after the mode has been set.
(defun remove-compilelog-buffer ()
  (if (get-buffer "*Compile-Log*")
      (kill-buffer "*Compile-Log*"))
  (delete-other-windows))
(add-hook 'emacs-startup-hook 'remove-compilelog-buffer)
;; (add-hook 'after-change-major-mode-hook 'remove-scratch-buffer)

;; Removes *messages* from the buffer.
;; (setq-default message-log-max nil)
;; (kill-buffer "*Messages*")

;; Removes *Completions* from buffer after you've opened a file.
(add-hook 'minibuffer-exit-hook
          '(lambda ()
             (let ((buffer "*Completions*"))
               (and (get-buffer buffer)
                    (kill-buffer buffer)))))


;; Full screen
(menu-bar-mode -1)
(custom-set-variables
 '(initial-frame-alist (quote (( fullscreen . fullscreen)))))
