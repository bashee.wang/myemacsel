;; -*- coding: utf-8 -*-
;; Time-stamp: <Chen Wang: 2018-05-29 16:50:19>

;; (setq py-smart-indentation nil)
;; (setq tab-width 4)
;; (setq-default py-indent-offset 4)
;; (setq-default python-indent-offset 4)
;; (setq indent-tabs-mode nil)
(setq python-indent-guess-indent-offset nil)
(add-hook 'python-mode-hook 
          (lambda () 
            (define-key python-mode-map "\C-m" 'newline-and-indent)))

;; Load elpy
(elpy-enable)
(setq elpy-rpc-backend "jedi")
;; (setq elpy-rpc-backend "rope")

;;enable jedi autocompletion in python
;; (add-hook 'python-mode-hook 'auto-complete-mode)
;; (add-hook 'python-mode-hook 'jedi:ac-setup)

(setq python-check-command "flake8") 

;;; Electric Pairs
(add-hook 'python-mode-hook
          (lambda ()
            (define-key python-mode-map "\"" 'electric-pair)
            (define-key python-mode-map "\'" 'electric-pair)
            (define-key python-mode-map "(" 'electric-pair)
            (define-key python-mode-map "[" 'electric-pair)
            (define-key python-mode-map "{" 'electric-pair)))
(defun electric-pair ()
  "Insert character pair without sournding spaces"
  (interactive)
  (let (parens-require-spaces)
    (insert-pair)))

;; Enable autopep8 with save action
(add-hook 'python-mode-hook 'py-autopep8-enable-on-save)
;; (define-key python-mode-map "\C-c\C-a" 'py-autopep8-buffer)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; SMART OPERATOR mode                   ;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(require 'py-smart-operator)
(add-hook 'python-mode-hook 'py-smart-operator-mode)

;; importmagic
(add-hook 'python-mode-hook 'importmagic-mode)

;; (require 'smart-operator)

;; (defun my-python-mode-common-hook()
;;   (smart-insert-operator-hook)

;;   (local-unset-key (kbd "."))
;;   (local-unset-key (kbd ":"))
;;   ;; (local-set-key (kbd "*") 'c-electric-star)
;;   )

;; (add-hook 'python-mode-hook 'my-python-mode-common-hook)

;; Python Info
;; (require 'pydoc-info)
;; (define-key python-mode-map "\C-c\C-b" 'python-shell-send-buffer)
;; (define-key python-mode-map (kbd "C-c s") 'info-lookup-symbol)
;; (define-key python-mode-map (kbd "C-c f") 'flymake-display-err-menu-for-current-line)

;; helm-pydoc
;; (with-eval-after-load "python"
;;   (define-key python-mode-map (kbd "C-c C-d") 'helm-pydoc))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(add-hook 'python-mode-hook 'my-python-hook)

(defun py-outline-level ()
  "This is so that `current-column` DTRT in otherwise-hidden text"
  ;; from ada-mode.el
  (let (buffer-invisibility-spec)
    (save-excursion
      (skip-chars-forward "\t ")
      (current-column))))

;; this fragment originally came from the web somewhere, but the outline-regexp
;; was horribly broken and is broken in all instances of this code floating
;; around. Finally fixed by Charl P. Botha <<a href="http://cpbotha.net/">http://cpbotha.net/</a>>
(defun my-python-hook ()
  (setq outline-regexp "[^ \t\n]\\|[ \t]*\\(def[ \t]+\\|class[ \t]+\\)")
  ;; enable our level computation
  (setq outline-level 'py-outline-level)
  ;; do not use their \C-c@ prefix, too hard to type. Note this overides
  ;; some python mode bindings
  (setq outline-minor-mode-prefix "\C-c2")
  ;; turn on outline mode
  (outline-minor-mode t)
  (linum-mode t)
  ;; (ac-ropemacs-setup)
  ;; initially hide all but the headers
  ;; (hide-body)
  (show-paren-mode 1)
  (rainbow-delimiters-mode 1)
  (rainbow-mode 1)
  )

;; (ido-mode 0)


;; activate pyvenv when start emacs.
(require 'pyvenv)
(pyvenv-activate "~/myprojects/django/djenv/")
;; (pyvenv-activate "~/myprojects/icecream/icenv/")
