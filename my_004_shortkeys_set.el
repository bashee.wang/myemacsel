;; -*- coding: utf-8 -*-
;; Time-stamp: <Chen Wang: 2019-01-29 18:09:54>

;; 设置一些全局的快捷键
;; (global-set-key (kbd "<C-tab>") (kbd "<M-tab>"))

(let ((binding (or (key-binding (kbd "<M-tab>")) 
                        (key-binding (kbd "M-TAB"))))) 
              (when binding 
                (global-set-key (kbd "<C-tab>") binding))) 

;; (global-set-key (kbd "<C-M-tab>") 'smart-tab)

(global-set-key [f1]     'auto-fill-mode)
(global-set-key [C-f1]   'calculator)
(global-set-key [S-f1]   'calendar)

(global-set-key [f2]     'highlight-phrase)
;; (global-set-key [C-f2]   'highlight-lines-matching-regexp)

(global-set-key [f3]     'ibuffer)
;; (global-set-key [C-f3]   'ediff-buffers)
;; (global-set-key [M-f3]   'ediff-directories)

(global-set-key [f4]     'kill-this-buffer)

(global-set-key [f5]  'color-rg-search-input)
(global-set-key [C-f5]  'color-rg-search-symbol)
(global-set-key [S-f5]  'deadgrep)
;; (global-set-key [f5]     'sql-oracle)
;; (global-set-key [S-f5]   'occur-by-moccur)

(global-set-key [f6]     'hl-line-mode)
;; (global-set-key [C-f6]   'column-highlight-mode)
;; (global-set-key [M-f6]   'crosshairs-mode)

(global-set-key [f7]     'occur)
(global-set-key [C-f7]   'moccur-grep-find)
(global-set-key [M-f7]   'dired-do-moccur)

(global-set-key [f8] 	 'dos-unix)
(global-set-key [C-f8] 	 'unix-dos)

(global-set-key [f9]     'conf-mode)
(global-set-key [C-f9]   'hs-minor-mode)

;(global-set-key [f10]    'ispell-word)
(global-set-key [C-f10]  'auto-complete-mode)
;; (global-set-key [M-f10]  'svn-examine)

(global-set-key [f11]    'linum-mode)
(global-set-key [M-f11]  'helm-locate)
(global-set-key [C-f11]  'helm-mini)

(global-set-key [f12]    'shell)
;; (global-set-key [M-f12]  'multi-occur)
;; (global-set-key [M-f12]  'recentf-open-files)

;; (global-set-key [mouse-4] 'next-buffer)
;; (global-set-key [mouse-5] 'previous-buffer)

(global-set-key (kbd "C-x C-g") 'goto-line)  ; goto line 
(global-set-key (kbd "C-c l") 'toggle-truncate-lines)
(global-set-key (kbd "C-x w") 'whitespace-mode)
(global-set-key (kbd "C-x RET d") 'describe-current-coding-system)
(global-set-key (kbd "C-c C-u") 'insert-char)
(global-set-key (kbd "C-c C-s") 'time-stamp)

;; 显示一个buffer list
;; (global-set-key (kbd "C-x C-b") 'bs-show)
;; 刷新文件 - 设置了自动revert-buffer之后可以不用这个快捷键了。
;; (global-set-key (kbd "C-c u") 'revert-buffer)
;; 这个命令配合 comment-dwim 基本上能满足所有的注释命令
;; (global-set-key (kbd "C-c g") 'comment-or-uncomment-region)
(global-set-key (kbd "C-;") 'comment-dwim)
;; view-mode 是用于浏览文件的一个很好用的 mode。
(global-set-key (kbd "C-c cv") 'view-mode)
;; restart emacs
(global-set-key (kbd "C-c rr") 'restart-emacs)
;; bing online dict
(global-set-key (kbd "C-c b") 'bing-dict-brief)
(global-set-key (kbd "C-c s") 'dictionary-search)
(global-set-key (kbd "C-c a") 'info-apropos)

(global-set-key [C-right] 'forward-sexp) 
(global-set-key [C-left] 'backward-sexp)

(global-set-key (kbd "C-x z") 'repeat-complex-command)

;; (global-set-key "\C-x\C-j" 'dired-jump) ; 通过 C-x C-j 跳转到当前目录的 Dired
