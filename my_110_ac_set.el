;; -*- coding: utf-8 -*-
;; Time-stamp: <Wang, Chen: 2016-04-15 01:08:28>

;; Basic Setup
(require 'auto-complete-config)
(global-auto-complete-mode t)
(ac-config-default)

(setq ac-fuzzy-enable t)
(setq ac-fuzzy-cursor-color "yellow")

(require 'pos-tip)
