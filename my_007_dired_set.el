;; -*- coding: utf-8 -*-
;;Time-stamp: <Chen Wang: 2018-10-09 17:46:31>

(require 'dired-filetype-face)

(add-hook 'dired-load-hook
          (function (lambda ()
                      (load "dired-x")
                      ;; Set global variables here.  For example:
                      ;; (setq dired-guess-shell-gnutar "gtar")
                      )))
(add-hook 'dired-mode-hook
          (function (lambda ()
                      ;; Set buffer-local variables here.  For example:
                      ;; (dired-omit-mode 1)
                      )))

;;;;;;;;;;About dired;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(setq dired-recursive-deletes `always) ; 可以递归的删除目录
(setq dired-recursive-copies `always)  ; 可以递归的进行拷贝
(require 'dired-x)               ; 有些特殊的功能
;; (global-set-key "\C-x\C-j" 'dired-jump) ; 通过 C-x C-j 跳转到当前目录的 Dired
;; Load wdired functions
(define-key dired-mode-map "r" 'wdired-change-to-wdired-mode)
;; 几个关于dired的常用自定义命令
(define-key dired-mode-map (kbd "C-c C-s") 'ywb-dired-filter-extension)
(define-key dired-mode-map (kbd "C-c C-r") 'ywb-dired-filter-regexp)

;; Setting up dired mode
(add-hook 'dired-mode-hook
          (lambda ()
            (define-key dired-mode-map "z" 'ywb-dired-compress-dir)
            ;; (define-key dired-mode-map "E" 'ywb-dired-w3m-visit)
            (define-key dired-mode-map "j" 'ywb-dired-jump-to-file)
            (define-key dired-mode-map "_" 'ywb-dired-count-dir-size)
            (define-key dired-mode-map "W" 'ywb-dired-copy-fullname-as-kill)
            ;; (define-key dired-mode-map "\C-q" 'ywb-dired-quickview)
            )
          )

;; 设置一些文件的默认打开方式，此功能必须在(require 'dired-x)之后 
(setq dired-guess-shell-alist-user
      `(("." (concat "start " "\"" (dired-get-filename) "\""))))

;;(defalias 'image-mode 'image-view-mode)
(setq dired-listing-switches "-alh")

;; (add-hook 'dired-after-readin-hook 'sof/dired-sort)
;; need externl el filed to support
;;; Sort dired mode: press s then s, x, t or n to sort by Size, eXtension, Time or Name
;;(load-file "~/.emacs.d/site-lisp/dired-sort-map.el")
;; (require 'dired-sort-map)

;; 方便的打开文件
;; (setq dired-guess-shell-alist-user
;;       (list '("\\.\\([mM][pP]3\\|ogg\\|wav\\|avi\\|mpg\\|dat\\|wma\\)$" "mplayer * &")
;;             '("\\.\\(gif\\|png\\|bmp\\|jpg\\|tif\\|JPG\\)$" "xzgv * &")
;;             '("\\.htm[l]?$" "firefox * &")
;;             '("\\.dvi$"     "xdvi * &")
;;             '("\\.rar$"     "unrar x * &")
;;             '("\\.pdf$"     "xpdf * &")
;;             '("\\.ppt$"     "start * &")))

;;;;;;;;;;;;;;;;;;;;;;dired-omit;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defvar sunway/oldreg-to-keep "")
(defvar sunway/oldreg-to-omit "")
(defvar sunway/mode-indicator "")
(defcustom sunway/reg-seprator "/"
  "seprator for reg"
  )
(make-variable-buffer-local 'sunway/oldreg-to-omit)
(make-variable-buffer-local 'sunway/oldreg-to-keep)
(make-variable-buffer-local 'sunway/mode-indicator)
(add-hook 'dired-after-readin-hook (lambda()
                                     (setq sunway/mode-indicator "")
                                     (unless (equal sunway/oldreg-to-keep "")
                                       (progn
                                         (sunway/dired-keep-or-omit-regexp nil
                                                                           sunway/oldreg-to-keep)
                                         (setq sunway/mode-indicator " KEEP")
                                         )
                                       )
                                     (unless (equal sunway/oldreg-to-omit "")
                                       (progn
                                         (sunway/dired-keep-or-omit-regexp 1
                                                                           sunway/oldreg-to-omit)
                                         (setq sunway/mode-indicator " KEEP")
                                         )
                                       )
                                     (force-mode-line-update)
                                     )
          )
(setq minor-mode-alist (add-to-list  'minor-mode-alist '(dired-omit-mode
                                                         sunway/mode-indicator)))
(defun sunway/dired-keep-or-omit-regexp (p reg)
  (interactive
   (list current-prefix-arg (if current-prefix-arg
                                (read-string "File to omit:" (concat
                                                              sunway/oldreg-to-omit sunway/reg-seprator) nil "")
                              (read-string "File to keep:" (concat
                                                            sunway/oldreg-to-keep sunway/reg-seprator) nil ""))
         ))
  (if p
      (setq sunway/oldreg-to-omit reg)
    (setq sunway/oldreg-to-keep reg)
    )
  (let ((sp (split-string reg sunway/reg-seprator))
        )
    (if p
        (mapc (lambda (str)
                (cond ((equal str "dir")
                       (dired-mark-directories nil)
                       (dired-do-kill-lines)
                       )
                      ((equal str "file")
                       (dired-mark-directories nil)
                       (dired-toggle-marks)
                       (dired-do-kill-lines)
                       )
                      ((not (equal str ""))
                       (dired-mark-files-regexp str)
                       (dired-do-kill-lines)
                       )
                      )
                )
              sp)
      (mapc (lambda (str)
              (cond ((equal str "dir")
                     (dired-mark-directories nil)
                     (dired-toggle-marks)
                     (dired-do-kill-lines)
                     )
                    ((equal str "file")
                     (dired-mark-directories nil)
                     (dired-do-kill-lines)
                     )
                    (t
                     (dired-mark-files-regexp str)
                     (dired-toggle-marks)
                     (dired-do-kill-lines)
                     )
                    )
              )
            sp)
      )
    )
  )

(define-key dired-mode-map "/" (lambda() (interactive) (call-interactively '
                                                        sunway/dired-keep-or-omit-regexp) (revert-buffer)))

;;;;;;;;;;;dired-print-full-path;;;;;;;;;;;;;;;;
(if (eq system-type 'windows-nt)
    (defun ywb-dired-copy-fullname-as-kill (&optional arg)
      "In dired mode, use key W to get the full name of the file"
      (interactive "P")
      (if arg
          (let ((string
                 (or (dired-get-subdir)
                     (mapconcat 'convert-standard-filename
                                (if arg
                                    (cond ((zerop (prefix-numeric-value arg))
                                           (dired-get-marked-files))
                                          ((consp arg)
                                           (dired-get-marked-files t))
                                          (t
                                           (dired-get-marked-files
                                            'no-dir (prefix-numeric-value arg)))
                                          )
                                  (dired-get-marked-files 'no-dir))
                                " "))))
            (if (eq last-command 'kill-region)
                (kill-append string nil)
              (kill-new string))
            (message string))
        (dired-copy-filename-as-kill 0)))
  (defun ywb-dired-copy-fullname-as-kill ()
    (interactive)
    (dired-copy-filename-as-kill 0)))

;; 用 tar 压缩 mark 的文件或者目录，在压缩文件上用这个命令则解压缩。
(defun ywb-dired-compress-dir ()
  (interactive)
  (let ((files (dired-get-marked-files t)))
    (if (and (null (cdr files))
             (string-match "\\.\\(tgz\\|tar\\.gz\\)" (car files)))
        (shell-command (concat "tar -xvf " (car files)))
      (let ((cfile (concat (file-name-nondirectory
                            (if (null (cdr files))
                                (car files)
                              (directory-file-name default-directory))) ".tgz")))
        (setq cfile
              (read-from-minibuffer "Compress file name: " cfile))
        (shell-command (concat "tar -zcvf " cfile " " (mapconcat 'identity files " ")))))
    (revert-buffer)))

;;; Jump to the file in dired-mode
(defun ywb-dired-jump-to-file ()
  "Quick jump to the file in dired-mode"
  (interactive)
  (goto-char (point-min))
  (dired-next-line 1)
  (let ((name "")
        input)
    (while (progn
             (setq input (read-char (format "Jump to: %s" name)))
             (if (and (< input ? ) (not (member input (list ?\d ?\^n ?\^P))))
                 ist last-input-event)))))

;;; 6. dired-about
(defun ywb-dired-filter-regexp (regexp &optional arg)
  (interactive
   (list (dired-read-regexp
          (concat (if current-prefix-arg "Exclude" "Exclude not")
                  " match (regexp): "))
         current-prefix-arg))
  (dired-mark-files-regexp regexp)
  (or arg (dired-toggle-marks))
  (dired-do-kill-lines))

(defun ywb-dired-filter-extension (extension &optional arg)
  "Filter by file extension. Multiple extension seperated by space, such as \"c cpp h\"."
  (interactive
   (list (read-from-minibuffer
          (concat "Exclude extension is "
                  (if current-prefix-arg "" "not") ": "))
         current-prefix-arg))
  (ywb-dired-filter-regexp
   (concat "\\.\\(" (regexp-opt (split-string extension)) "\\)\\'")
   arg))

;;; 14 count folder size
(defun ywb-dired-count-dir-size ( arg )
  (interactive "P")
  (let ((dir (dired-get-filename nil t))
        proc)
    (when (file-directory-p dir)
      (with-current-buffer (get-buffer-create "*Shell Command Output*")
        (erase-buffer)
        (setq proc
              (start-process-shell-command "dirsize" (current-buffer)
                                           "du"
                                           "-h"
                                           (if arg "" "-s")
                                           (format "\"%s\"" dir)))
        (set-process-sentinel proc
                              (lambda (proc event)
                                (let ((buf (process-buffer proc)))
                                  (message "Done!")
                                  (unless (get-buffer-window buf)
                                    (display-buffer buf))
                                  (with-selected-window
                                      (get-buffer-window buf)
                                    (reverse-region (point-min) (point-max))
                                    (goto-char (point-min))))))
        (message "Wait while count size...")))))

;;; dired some small functions
(defun ywb-dired-w3m-visit ()
  (interactive)
  (w3m-goto-url (concat "file://" (dired-get-filename nil t))))

(defun ywb-dired-copy-fullname-as-kill ()
  (interactive)
  (dired-copy-filename-as-kill 0))


;; 支持中文拼音跳到中文文件
(defun ywb-hanstr-to-py (str)
  (require 'quail)
  (unless (quail-package "chinese-py")
    (load "quail/PY"))
  (let ((quail-current-package (quail-package "chinese-py")))
    (mapconcat (lambda (char)
                 (car (quail-find-key char)))
               (append str nil) "")))

(defun ywb-dired-jump-to-file ()
  "Quick jump to the file in dired-mode"
  (interactive)
  (goto-char (point-min))
  (dired-next-line 1)
  (let ((name "")
        input)
    (while (progn
             (setq input (read-char (format "Jump to: %s" name)))
             (if (and (< input ?\s) (not (member input (list ?\d ?\^n ?\^P))))
                 (progn (setq unread-command-events (list input)) nil)
               (cond ((= input ?\d)
                      (progn (setq name (substring name 0 -1))
                             (goto-char (point-min))
                             (dired-next-line 1)))
                     ((= input ?\^N)
                      (dired-next-line 1))
                     ((= input ?\^P)
                      (while (progn
                               (dired-previous-line 1)
                               (not (or (looking-at name) (bobp))))))
                     (t
                      (setq name (concat name (char-to-string input)))))
               (while (not (or (eobp)
                               (let ((fn (dired-get-filename t t)))
                                 (and fn
                                      (string-match (concat "^" (regexp-quote
                                                                 name))
                                                    (replace-regexp-in-string
                                                     "\\cC" 'ywb-hanstr-to-py fn))))))
                 (dired-next-line 1))
               t)))))

