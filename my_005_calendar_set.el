;; -*- coding: utf-8 -*-
;; Time-stamp: <Chen Wang: 2018-12-11 10:42:54>

;; Mark weekends days with a different color.
(defadvice calendar-generate-month
    (after highlight-weekend-days (month year indent) activate)
  "Highlight weekend days"
  (dotimes (i 31)
    (let ((date (list month (1+ i) year)))
      (if (or (= (calendar-day-of-week date) 0)
              (= (calendar-day-of-week date) 6))
          (calendar-mark-visible-date date 'font-lock-doc-string-face)))))

;; Add week number
(copy-face font-lock-constant-face 'calendar-iso-week-face)
(set-face-attribute 'calendar-iso-week-face nil
                    :height 0.7)
(setq calendar-intermonth-text
      '(propertize
        (format "%2d"
                (car
                 (calendar-iso-from-absolute
                  (calendar-absolute-from-gregorian (list month day year)))))
        'font-lock-face 'calendar-iso-week-face))

(copy-face 'default 'calendar-iso-week-header-face)
(set-face-attribute 'calendar-iso-week-header-face nil
                    :height 0.8)
(setq calendar-intermonth-header
      (propertize "Wk"                  ; or e.g. "KW" in Germany
                  'font-lock-face 'calendar-iso-week-header-face))

;; Set Monday as 1st day of a week
(setq calendar-week-start-day 1)


;; Set diary
(setq diary-file "~/.emacs.d/myconfig/mydiary.gpg")
(setq view-diary-entries-initially t
      mark-diary-entries-in-calendar t
      number-of-diary-entries 7)
(add-hook 'diary-display-hook 'fancy-diary-display)
(add-hook 'today-visible-calendar-hook 'calendar-mark-today)

;; (add-hook 'diary-display-hook 'fancy-diary-display)
;; (add-hook 'list-diary-entries-hook 'sort-diary-entries t)

;; to get rid of the ugly equal signs under the date:
(add-hook 'fancy-diary-display-mode-hook
	  '(lambda ()
             (alt-clean-equal-signs)))

(defun alt-clean-equal-signs ()
  "This function makes lines of = signs invisible."
  (goto-char (point-min))
  (let ((state buffer-read-only))
    (when state (setq buffer-read-only nil))
    (while (not (eobp))
      (search-forward-regexp "^=+$" nil 'move)
      (add-text-properties (match-beginning 0) 
	                   (match-end 0) 
			   '(invisible t)))
    (when state (setq buffer-read-only t))))
